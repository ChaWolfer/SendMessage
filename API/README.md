# API REST

## Description

Projet d'api rest développé à Pop School, durant la session 2016-2017.

L'api fournit une liste de message.

## Structure de données

message :

- object : string
- message : string
- author : string
- dateEdit : date


## Installation des dépendances

    composer install

## Création de la base de données

Exécutez le script `db.sql` qui se trouve dans le dossier `data`

    cd data
    mysql -u root -p < db.sql

## Utilisation

Démarrez un serveur web de développement dans le dossier `web`

    cd web
    php -S localhost:8000

### GET /

Renvoie la doc de l'api

Ouvrez cette page dans un navigateur web

### GET /messages

Renvoie la liste des messages

Exemple avec httpie :

    http localhost:8000/messages/

### GET /messages/{id}

Renvoie le détail d'un message

Exemple avec httpie :

    http localhost:8000/messages/1

### POST /messages

Ajoute un message

Exemple avec httpie :

    http --json localhost:8000/messages/ object=Foo2 message=Bar author=Bar dateEdit=2017-02-01

### PUT /messages/{id}

Modifie un message

Exemple avec httpie :

    http --json PUT localhost:8000/messages/2 object=Lorem message=Ipsum author=Mozart dateEdit=2000-12-01

### DELETE /messages/{id}

Supprime un message

Exemple avec httpie :

    http DELETE localhost:8000/messages/1

## Exceptions

En cas d'erreur, l'api renvoie le code http 500 et un objet json ayant la structure suivante :

    {
        "code": 0,
        "error": true,
        "message": "message d'erreur"
    }

ou celle-ci :

    {
        "code": 0,
        "error": true,
        "message": [
            "message d'erreur 1",
            "message d'erreur 2",
            "message d'erreur 3"
        ]
    }

## Déboggage

Pour voir la requête HTTP envoyée au serveur ajouter l'option `--verbose` à votre commande :

- commande originale

    http --json localhost:8000/messages/ author="Foo Bar"

- commande de déboggage

    http --verbose --json localhost:8000/messages/ message="Foo Bar"

Pour « décrypter » un message d'erreur html, ajoutez l'option `--body` à votre commande et redirigez la sortie standard vers un fichier avec `>` :

- commande originale

    http --json localhost:8000/messages/ author="Foo Bar"

- commande de déboggage

    http --body --json localhost:8000/messages/ author="Foo Bar" > debug.html
